const { Observable} = require('rxjs/Rx');

const observable = Observable.create(function (observer) {
  observer.next(1);
  observer.next(2);
  observer.next(3);
  setTimeout(() => {
    observer.next(4);
    observer.error(123)
    observer.complete();
  }, 1000);
});

const observer = {
  next: x => console.log(x),
  error: error => console.error('error'),
  complete: () => console.log('complete'),
};

observable.subscribe(observer);