const { Observable, Subject } = require('rxjs/Rx');

const observable = Observable.create(function (observer) {
  observer.next(1);
  observer.next(2);
  observer.next(3);
  setTimeout(() => {
    observer.next(4);
    observer.complete();
  }, 1000);
});

const observer = {
  next: x => console.log(x),
  error: error => console.error('error'),
  complete: () => console.log('complete'),
};

observable.subscribe(observer);

const subject = new Subject();

subject.subscribe({
  next: (v) => console.log('observerA: ' + v)
});
subject.subscribe({
  next: (v) => console.log('observerB: ' + v)
});

observable.subscribe(subject);